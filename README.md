# Timestamp stretcher

Stretch integer time stamps by a fractional factor that may change over time.
This library takes care to avoid time-drifting and keep the errors small.

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md) for more information.

## License

`timestamp-stretcher` is licensed under the Apache License, Version 2.0 
or the MIT license, at your option.

For the application of the MIT license, the examples included in the doc comments are not
considered "substantial portions of this Software".

