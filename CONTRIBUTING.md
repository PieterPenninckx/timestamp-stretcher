# Contributing

Before contributing, it's a good idea to have a look at the open issues
and to open an issue or contact us via e-mail to ensure there is no double-work
and that your contribution is likely to be accepted.

## How to contribute?
It's most convenient for me if you create a pull request, but you can also send your changes via e-mail.

## The legal side
Well, this is clearly a hobby project. 
Chances are low that anybody is actually going to create a legal mess about a piece of
software with probably less than five users.
Nevertheless, let's try to do it right.

The Linux Foundation created the [Developer Certificate of Origin](https://developercertificate.org/).
This can prevent some legal problems, and it's also fair towards the contributors:
everybody can use the project under the same terms and conditions.

In order to contribute to `timestamp-stretcher`, you need to agree with the Developer Certificate of Origin,
which you can find below or on [https://developercertificate.org/](https://developercertificate.org/).

```text

Developer's Certificate of Origin 1.1

By making a contribution to this project, I certify that:

(a) The contribution was created in whole or in part by me and I
    have the right to submit it under the open source license
    indicated in the file; or

(b) The contribution is based upon previous work that, to the best
    of my knowledge, is covered under an appropriate open source
    license and I have the right under that license to submit that
    work with modifications, whether created in whole or in part
    by me, under the same open source license (unless I am
    permitted to submit under a different license), as indicated
    in the file; or

(c) The contribution was provided directly to me by some other
    person who certified (a), (b) or (c) and I have not modified
    it.

(d) I understand and agree that this project and the contribution
    are public and that a record of the contribution (including all
    personal information I submit with it, including my sign-off) is
    maintained indefinitely and may be redistributed consistent with
    this project or the open source license(s) involved.
```

When contributing with `git`, you can express that you agree to the
Developer Certificate of Origin by including a DCO
in the commit message as follows: "`Signed-off-by:` _your name_ _your e-mail address_".
(Tip: see the documentation of the `--signoff` flag of `git`.)

When contributing over e-mail, you can express that you agree to the
Developer Certificate of Origin by including the following statement in the e-mail message:
"`Signed-off-by:` _your name_ _your e-mail address_".

Don't forget to add your name to the [COPYRIGHT](COPYRIGHT) file as well.
Feel free to add your name to the [Cargo.toml](Cargo.toml) file.
