//     Stretch integer time stamps by a fractional factor that may change over time.
//
//     Copyright (C) 2021 Pieter Penninckx
//
//     `timestamp-stretcher` is licensed under the Apache License, Version 2.0
//     or the MIT license, at your option.
//
//     For the application of the MIT license, the examples included in the doc comments are not
//     considered "substantial portions of this Software".
//
//     License texts can be found:
//     * for the Apache License, Version 2.0: <LICENSE-APACHE.txt> or
//         <http://www.apache.org/licenses/LICENSE-2.0>
//     * for the MIT license: <LICENSE-MIT.txt> or
//         <http://opensource.org/licenses/MIT>.
//
//
//! # `TimeStretcher`
//! Stretch integer (`u64`) time stamps by a fractional factor that may change over time.
//!
//! ## Examples
//! The following example illustrates the usage of the library.
//! ```
//! use timestamp_stretcher::TimestampStretcher;
//! use std::num::NonZeroU64;
//!
//! // Create a new TimeStretcher with an initial factor of 2/3.
//! let mut stretcher = TimestampStretcher::new(2, NonZeroU64::new(3).unwrap());
//!
//! // Suppose we have five events: "0", "1", "2", "3" and "4".
//! // Each event happens at a certain time stamp:
//! //       +--- Time stamp of event "1" is 3
//! //       v
//! // 0 . . 1 . . 2 . . . . 3 . . . . 4
//! // If we stretch the time stamps up to event 2 with a factor 2/3,
//! // and the subsequent time stamps with a factor 7/5, we get the following:
//! // 0 . 1 . 2 . . . . . . 3 . . . . . . 4
//! let input_time_stamps = vec![
//!     (0, None),                                     // Event at time 0, no change to conversion factor.
//!     (3, None),                                     // Event at time 3, no change to conversion factor.
//!     (6, Some((7, NonZeroU64::new(5).unwrap()))),   // Event at time 6, change conversion factor to 7/8.
//!     // The new conversion factor will be applied to the time differences, not to the absolute times.
//!     (11, None),                                    // Event at time 11.
//!     (16, None),
//! ];
//! let mut observed_output_time_stamps = Vec::new();
//! for input in input_time_stamps.into_iter() {
//!     observed_output_time_stamps.push(stretcher.stretch(input.0, input.1));
//! }
//! let expected_output_time_stamps = vec![0, 2, 4, 11, 18];
//! assert_eq!(expected_output_time_stamps, observed_output_time_stamps);
//!```
//!

use core::num::NonZeroU64;
use gcd::Gcd;

/// Stretch integer (`u64`) time stamps by a fractional factor that may change over time.
pub struct TimestampStretcher {
    nominator: u64,
    denominator: NonZeroU64,
    input_offset: u64,
    output_offset: u64,
    accumulated_error: f64,
    correction_term: u64,
}

impl TimestampStretcher {
    /// Create a new `TimestampStretcher`.
    ///
    /// # Example
    /// ```
    /// use std::num::NonZeroU64;
    /// use timestamp_stretcher::TimestampStretcher;
    ///
    /// let mut stretcher = TimestampStretcher::new(2, NonZeroU64::new(3).unwrap());
    /// assert_eq!(10, stretcher.stretch(15, None));
    /// ```
    pub fn new(nominator: u64, denominator: NonZeroU64) -> Self {
        let (nominator, denominator) = Self::simplify(nominator, denominator);
        let denominator_u64: u64 = denominator.into();
        Self {
            nominator,
            denominator,
            input_offset: 0,
            output_offset: 0,
            accumulated_error: 0.0,
            correction_term: denominator_u64 / 2,
        }
    }

    fn simplify(nominator: u64, denominator: NonZeroU64) -> (u64, NonZeroU64) {
        let denominator: u64 = denominator.into(); // Avoid cumbersome type annotations.
        let gcd = nominator.gcd(denominator);
        let simplified_nominator = nominator / gcd;
        // Because denominator != 0, gcd != 0.
        // gcd <= denominator, denominator / gcd >= 1 > 0.
        let simplified_denominator =
            NonZeroU64::new(denominator / gcd).expect("Bug: number should not be zero.");
        (simplified_nominator, simplified_denominator)
    }

    /// Stretch the input time by the factor `nominator/denominator`.
    ///
    /// # Parameters
    /// `input_time`: the absolute time to be stretched. It is assumed that this only increases
    ///     with subsequent calls to this method.
    /// `new_conversion_factor`: if this contains `Some((nominator, denominator))`,
    /// future times between successive events
    /// will be stretched by the new factor `nominator/denominator`.
    ///
    /// # Returns
    /// The absolute output time.
    ///
    /// # Panics
    /// May panic if the input time is `<` the input time that was passed in the previous call to
    /// this method on the same object.
    ///
    /// # Example
    /// ```
    /// use std::num::NonZeroU64;
    /// use timestamp_stretcher::TimestampStretcher;
    ///
    /// let mut stretchor = TimestampStretcher::new(2, NonZeroU64::new(3).unwrap());
    /// assert_eq!(10, stretchor.stretch(15, Some((1, NonZeroU64::new(1).unwrap()))));
    /// assert_eq!(11, stretchor.stretch(16, None));
    /// ```
    ///
    /// # Drifting correction and rounding error.
    /// The output is rounded to an integer value.
    /// Care is taken to keep the rounding error small:
    /// if `e` is the rounding error, then  `|e| < 1/2 + n ϵ`,
    /// where `ϵ` is the machine precision of an `f64`
    /// and `n` is the number of changes to the conversion factor.
    pub fn stretch(
        &mut self,
        absolute_input_time: u64,
        new_conversion_factor: Option<(u64, NonZeroU64)>,
    ) -> u64 {
        let self_denominator: u64 = self.denominator.into(); // Avoid some cumbersome type annotations.

        // Adapt offsets.
        let input_offset_delta: u64 = (absolute_input_time - self.input_offset) / self_denominator;
        self.input_offset += input_offset_delta * self_denominator;
        self.output_offset += input_offset_delta * self.nominator;

        let new_time = ((absolute_input_time - self.input_offset) * self.nominator
            + self.correction_term)
            / self_denominator
            + self.output_offset;

        if let Some((new_nominator, new_denominator)) = new_conversion_factor {
            let new_denominator_u64: u64 = new_denominator.into(); // Avoid some cumbersome type annotations.

            let gcd = new_nominator.gcd(new_denominator_u64);
            let simplified_new_nominator = new_nominator / gcd;
            // Because new_denominator != 0, gcd != 0.
            // gcd <= new_denominator, new_denominator / gcd >= 1 > 0.
            let simplified_new_denominator_u64: u64 = new_denominator_u64 / gcd;
            let simplified_new_denominator = NonZeroU64::new(simplified_new_denominator_u64)
                .expect("Bug: number should not be zero.");

            if simplified_new_nominator != self.nominator
                || simplified_new_denominator != self.denominator
            {
                // Update the accumulated error.
                self.accumulated_error += ((absolute_input_time - self.input_offset)
                    * self.nominator) as f64
                    / (self_denominator as f64)
                    - ((new_time - self.output_offset) as f64);
                debug_assert!(self.accumulated_error.abs() <= 0.5);

                // Set the correction term, to compensate for the accumulated error.
                self.correction_term =
                    ((0.5 + self.accumulated_error) * simplified_new_denominator_u64 as f64) as u64;

                // Set the new offsets. This is what causes the error to accumulate.
                self.input_offset = absolute_input_time;
                self.output_offset = new_time;

                // Set the new nominators and denominators.
                self.nominator = simplified_new_nominator;
                self.denominator = simplified_new_denominator;
            }
        }
        new_time
    }
}

#[test]
pub fn timestamp_stretcher_takes_error_into_account_when_rounding() {
    fn factor_change(nominator: u64, denominator: u64) -> Option<(u64, NonZeroU64)> {
        Some((nominator, NonZeroU64::new(denominator).unwrap()))
    }
    let mut stretcher = TimestampStretcher::new(1, NonZeroU64::new(1).unwrap());
    // input => output = formula, explanation
    // 0 => 0, here we change factor to 1/5
    // 2 => 0 = round(2 * factor), the error is 2/5, we switch factor to 2/5, accumulated error is 2/5
    // 3 => 2 = 0 + round(((3 - 2) + 2) * factor)
    let input = vec![
        (0, factor_change(1, 5)),
        (2, factor_change(2, 5)),
        (3, None),
    ];
    let mut observed_output = Vec::new();
    for input in input.into_iter() {
        observed_output.push(stretcher.stretch(input.0, input.1));
    }
    let expected_output = vec![0, 0, 1];
    assert_eq!(expected_output, observed_output);
}

#[test]
pub fn timestamp_stretcher_first_stretching_and_then_compressing_gives_input_mostly() {
    let mut stretcher = TimestampStretcher::new(15, NonZeroU64::new(7).unwrap());
    let mut compressor = TimestampStretcher::new(7, NonZeroU64::new(15).unwrap());
    for input in 0..100 {
        let stretched = stretcher.stretch(input, None);
        let observed = compressor.stretch(stretched, None);
        assert_eq!(observed, input);
    }
}

#[cfg(test)]
struct TestTimestampStretcher {
    factor: f64,
    previous_input_time: f64,
    previous_output_time: f64,
}

#[cfg(test)]
impl TestTimestampStretcher {
    pub fn new(nominator: u64, denominator: NonZeroU64) -> Self {
        let denominator: u64 = denominator.into();
        Self {
            factor: nominator as f64 / denominator as f64,
            previous_input_time: 0.0,
            previous_output_time: 0.0,
        }
    }

    pub fn stretch(
        &mut self,
        absolute_input_time: u64,
        new_conversion_factor: Option<(u64, NonZeroU64)>,
    ) -> u64 {
        let absolute_input_time: f64 = absolute_input_time as f64;
        let delta = absolute_input_time - self.previous_input_time;
        self.previous_input_time = absolute_input_time;
        let new_time = self.previous_output_time + delta * self.factor;
        self.previous_output_time = new_time;

        if let Some((new_nominator, new_denominator)) = new_conversion_factor {
            let new_denominator: u64 = new_denominator.into();
            self.factor = new_nominator as f64 / new_denominator as f64;
        }
        (new_time + 0.5) as u64
    }
}

#[test]
pub fn floating_point_stretchers_and_real_stretcher_match_for_small_data_and_lots_of_factor_changes(
) {
    fn factor_change(nominator: u64, denominator: u64) -> Option<(u64, NonZeroU64)> {
        Some((nominator, NonZeroU64::new(denominator).unwrap()))
    }
    let mut stretcher = TimestampStretcher::new(1, NonZeroU64::new(1).unwrap());
    let mut test_stretcher = TestTimestampStretcher::new(1, NonZeroU64::new(1).unwrap());

    let input = vec![
        (0, factor_change(2, 3)),
        (1, factor_change(5, 3)),
        (2, factor_change(5, 7)),
        (3, factor_change(11, 7)),
        (5, factor_change(13, 11)),
        (8, factor_change(17, 13)),
        (10, factor_change(19, 17)),
        (16, factor_change(29, 23)),
        (40, factor_change(31, 37)),
        (50, None),
    ];
    let mut real_output = Vec::new();
    let mut test_output = Vec::new();
    for input in input.into_iter() {
        real_output.push(stretcher.stretch(input.0, input.1));
        test_output.push(test_stretcher.stretch(input.0, input.1));
    }
    assert_eq!(real_output, test_output);
}

#[test]
#[ignore = "This test eats your CPU, but you can run it if you want (takes less then a minute to run)."]
pub fn using_integers_does_not_give_rounding_errors_for_large_numbers() {
    let mut test_stretcher = TimestampStretcher::new(1, NonZeroU64::new(3).unwrap());
    let _1e8 = 100_000_000;
    test_stretcher.stretch(3 * _1e8, None);
    for i in 0.._1e8 {
        test_stretcher.stretch(3 * _1e8 + 3 * i + 1, None);
        test_stretcher.stretch(3 * _1e8 + 3 * i + 2, None);
        let result = test_stretcher.stretch(3 * _1e8 + 3 * i + 3, None);
        assert_eq!(result, _1e8 + i + 1);
    }
}

#[test]
#[ignore = "This test eats your CPU and is only there to prove my point, so I'm ignoring this."]
pub fn using_floating_points_gives_rounding_errors_for_large_numbers() {
    let mut test_stretcher = TestTimestampStretcher::new(1, NonZeroU64::new(3).unwrap());
    let _1e8 = 100_000_000;
    test_stretcher.stretch(3 * _1e8, None);
    let mut number_of_errors: u64 = 0;
    for i in 0.._1e8 {
        test_stretcher.stretch(3 * _1e8 + 3 * i + 1, None);
        test_stretcher.stretch(3 * _1e8 + 3 * i + 2, None);
        let result = test_stretcher.stretch(3 * _1e8 + 3 * i + 3, None);
        if result != _1e8 + i + 1 {
            number_of_errors += 1;
        }
    }
    assert_ne!(number_of_errors, 0);
}
